#ifndef LINEMAKER_H
#define LINEMAKER_H

#include <qvector.h>

#include <line.h>
#include <obstacle.h>

class LineMaker
{
public:
    int current_direction;
    /*
    int last_direction;
    int probe_flag;
    int current_ok_flag;
    int last_ok_flag;
    */

    Point current_point;

    QVector<Line> line_stack;
    Point init_point;
    Point terminal_point;

    std::vector<Obstacle> ob_vector;
    ObstacleTable ob_x_table;
    ObstacleTable ob_y_table;

    LineMaker(Point init, Point term, std::vector<Obstacle> ob_vector);
    void getNextPoint();
    int awayOb(int a, int b);
    void retry();
    int isArrive();
    int Try();//iterate
    void clear();

    //output
    void showLine();
    int showLenth();
};

#endif // LINEMAKER_H
