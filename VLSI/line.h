#ifndef LINE_H
#define LINE_H

#include <point.h>

class Line
{
public:
    int direction;
    int lenth;
    Point start_point;
    Point end_point;
    Line* next_line;
    Line* short_line;


    Line(Point st, Point en);
    Line();
    void getDirection();
    void makeShort();
};


#endif // LINE_H
