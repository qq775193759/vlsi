#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qpainter.h>

#include <linemaker.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void paintEvent(QPaintEvent *);

private slots:
    void on_actionClear_triggered();

    void on_pushButton_add_clicked();

    void on_action_entersrc_triggered();

    void on_action_enterdest_triggered();

private:
    Ui::MainWindow *ui;

    LineMaker* linemaker;

    void drawLine(Line l);
    void drawOb(Obstacle o);
    void drawPoi(Point p);
    void drawAll();
};

#endif // MAINWINDOW_H
