#include "obstacle.h"
#include <iostream>
#include <algorithm>

using namespace std;

ObstacleTable::ObstacleTable(vector<Obstacle> originObs, int boundX, int boundY, int edge, int mode)
{
	for(unsigned int i=0; i<originObs.size(); i++)
		if(mode == 0)
		{
			obs.push_back(Obstacle_std(originObs[i].y1, originObs[i].y2, originObs[i].x1, originObs[i].x2));
			bounda = boundY;
			boundb = boundX;
		}
		else
		{
			obs.push_back(Obstacle_std(originObs[i].x1, originObs[i].x2, originObs[i].y1, originObs[i].y2));
			bounda = boundX;
			boundb = boundY;
		}
	N = obs.size();
	_edge = edge;	
	_mode = mode;

	initTable();
	//cout<<table.size()<<endl;
	initSpace();
	/*for(int i=0; i<table.size(); i++)
		cout<<table[i].space.size()<<endl;
	cout<<"---"<<endl;*/
}
void ObstacleTable::initTable()
{
	int *apos = new int[obs.size()*2+2];
	int n = 0;
	apos[n++] = _edge;
	for(unsigned int i=0; i<obs.size(); i++)
	{
		apos[n++] = obs[i].a1;
		apos[n++] = obs[i].a2;
	}
	apos[n++] = bounda;
	sort(apos, apos+n);//
	int last = _edge;
    for(int i=0; i<n; i++)
	{
		if(apos[i] == last)
			continue;
		else
		{
			table.push_back(TableNode(last, last));
			table.push_back(TableNode(last, apos[i]));
			last = apos[i];
		}
	}
	table.push_back(TableNode(last, last));
	delete[] apos;
}
/*
bool ObstacleTable::compare(int op1, int op2)
{
	return (*obs_st)[op1].b1 < (*obs_st)[op2].b1;
}
*/
bool ObstacleTable::intersect(int pos, Obstacle_std& ob)
{
	if(pos > ob.a1 && pos < ob.a2)
		return true;
	else
		return false;
}
void ObstacleTable::initSpace()
{
	int* bobs = new int[N];
	for(int i=0; i<N; i++)
		bobs[i] = i;

	//sort(bobs, bobs+N, compare);
	for(int i=0; i<N-1; i++)
		for(int j=i+1; j<N; j++)
			if(obs[bobs[i]].b1 > obs[bobs[j]].b1)
			{
				int tmp = bobs[i];
				bobs[i] = bobs[j];
				bobs[j] = tmp;
			}

	for(unsigned int i=0; i<table.size(); i++)
	{
		int pos = (table[i].lo + table[i].hi)/2;
		int lastId = -1;
		int lastPos = _edge;
		for(int j=0; j<N; j++)
		{
			Obstacle_std& ob = obs[bobs[j]];
			if(intersect(pos, ob))
			{
				table[i].space.push_back(ObstacleSpace(lastPos, ob.b1, lastId, bobs[j]));
				lastPos = ob.b2;
				lastId = bobs[j];
			}
		}
		if(lastPos < boundb)
			table[i].space.push_back(ObstacleSpace(lastPos, boundb, lastId, -1));
	}
}
pair<int,int> ObstacleTable::findNeighborOb(int curX, int curY)
{
	if(_mode == 0)
		return findOb(curY, curX);
	else
		return findOb(curX, curY);
}
pair<int,int> ObstacleTable::findOb(int A, int B)
{
	int left = 0, right = table.size();
	int med;
	while(left <= right)
	{
		med = (left + right) / 2;
		if(table[med].lo > A) 
			right = med - 1;
		else if(table[med].hi < A)
			left = med + 1;
		else
		{
			if(table[med].lo != table[med].hi)
				if(A == table[med].lo) 
					med = med - 1;
				else if(A == table[med].hi)
					med = med + 1;
			break;
		}
	}
	
	vector<ObstacleSpace> space = table[med].space;
	left = 0, right = space.size();
	bool find = false;
	while(left <= right)
	{
		med = (left + right) / 2;
		if(space[med].lo > B)
			right = med - 1;
		else if(space[med].hi < B)
			left = med + 1;
		else 
		{
			find = true;
			break;
		}
	}
	if(find)
		return pair<int,int>(space[med].leftId, space[med].rightId);
	else
		return pair<int,int>(-2, -2);
}
/*
int main()
{
	vector<Obstacle> obs;
	for(int i=0; i<3 ;i++)
		for(int j=0; j<2; j++)
			obs.push_back(Obstacle(2+5*i, 5+5*i, 2+5*j, 5+5*j));
	ObstacleTable xtable = ObstacleTable(obs, 17, 12, 0, 0);
	ObstacleTable ytable = ObstacleTable(obs, 17, 12, 0, 1);
	pair<int,int> res = xtable.findNeighborOb(3, 3);
	cout<<res.first<<' '<<res.second<<endl;
	res = ytable.findNeighborOb(3, 3);
	cout<<res.first<<' '<<res.second<<endl;
	system("pause");
}*/
