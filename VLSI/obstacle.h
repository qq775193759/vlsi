#ifndef OBSTACLE_H
#define OBSTACLE_H
#include <vector>

struct Obstacle
{
	int x1;
	int x2;
	int y1;
	int y2;
	Obstacle(int _x1, int _x2, int _y1, int _y2):x1(_x1), x2(_x2), y1(_y1), y2(_y2){}
};

struct Obstacle_std
{
	int a1;
	int a2;
	int b1;
	int b2;
	Obstacle_std(int _a1, int _a2, int _b1, int _b2):a1(_a1), a2(_a2), b1(_b1), b2(_b2){}
};

struct ObstacleSpace
{
	int lo;
	int hi;
	int leftId;
	int rightId;
	ObstacleSpace(int l, int h, int left, int right):lo(l), hi(h), leftId(left), rightId(right){}
};

struct TableNode
{
	int lo;
	int hi;
	std::vector<ObstacleSpace> space;
	TableNode(int l, int h):lo(l), hi(h){}
};

class ObstacleTable
{
public:
	/*
	boundX: with bubble max X
	boundY: with bubble max Y
	edge:	bubble width
	mode:	0 Xtable 1 Ytable
	*/
	ObstacleTable(std::vector<Obstacle>, int, int, int, int);
	std::pair<int,int> findNeighborOb(int curX, int curY);
private:
	int _edge;
	int _mode;
	int N;
	std::vector<Obstacle_std> obs;
	int bounda;
	int boundb;
	std::vector<TableNode> table;
	
	void initTable();
	void initSpace();
	bool intersect(int, Obstacle_std&);
	std::pair<int,int> findOb(int A, int B);
};

#endif
