#include "line.h"
#include <math.h>

Line::Line(Point st, Point en)
{
    start_point = st;
    end_point = en;
    getDirection();
    next_line = 0;
}

Line::Line(){}

void Line::getDirection()
{
    if(start_point.x == end_point.x)
    {
        direction = 1;//
        int temp_len = start_point.y - end_point.y;
        lenth = abs(temp_len);

        if(lenth < 10) return;

        Point short_line_point(end_point.x, end_point.y + 10*temp_len/lenth);
        short_line = new Line(start_point, short_line_point);
    }
    else if(start_point.y == end_point.y)
    {
        direction = 0;//
        int temp_len = start_point.x - end_point.x;
        lenth = abs(temp_len);

        if(lenth < 10) return;

        Point short_line_point(end_point.x + 10*temp_len/lenth, end_point.y);
        short_line = new Line(start_point, short_line_point);
    }
    else
    {
        direction = -1;
    }
}

void Line::makeShort()
{
    if(lenth < 10) return;
    next_line = short_line;
}
