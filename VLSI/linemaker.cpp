#include "linemaker.h"
#include <qdebug.h>
#include <math.h>
#include <QFile>


LineMaker::LineMaker(Point init, Point term, std::vector<Obstacle> ob_vector)
    :ob_vector(ob_vector),
     ob_x_table(ob_vector, 1495, 1495, 5, 0),
     ob_y_table(ob_vector, 1495, 1495, 5, 1)
{
    init_point = init;
    terminal_point = term;
    current_point = init_point;

    current_direction = 0;
}

void LineMaker::clear()
{
    line_stack.clear();
    current_point = init_point;
    current_direction = 0;
}

int LineMaker::Try()
{
    if(isArrive()) return 1;
    getNextPoint();
    if( Try() ) return 1;
    return 0;
}

void LineMaker::getNextPoint()
{
    if(current_direction == 0)
    {
        std::pair<int,int> temp_pair = ob_x_table.findNeighborOb(current_point.x, current_point.y);
        int temp_ob;
        int ob_kind;
        //qDebug() << temp_pair.first << temp_pair.second;
        if(terminal_point.x > current_point.x)
        {
            temp_ob = temp_pair.second;
            ob_kind = 2;
        }
        else
        {
            temp_ob = temp_pair.first;
            ob_kind = 1;
        }
        //make line
        if(temp_ob == -1)
        {
            current_direction = 1;
            Point next_point(terminal_point.x, current_point.y);
            Line new_line(current_point, next_point);
            new_line.makeShort();
            line_stack.push_back(new_line);
            current_point = next_point;
        }
        else
        {
            Obstacle ob = ob_vector[temp_ob];
            int next_x = (ob_kind==2)?ob.x1:ob.x2;
            Point next_point(next_x, current_point.y);
            Line new_line(current_point, next_point);
            new_line.makeShort();
            line_stack.push_back(new_line);
            current_point = next_point;
            current_direction = 1;

            awayOb(ob.y1, ob.y2);
        }
    }
    else if(current_direction == 1)
    {
        std::pair<int,int> temp_pair = ob_y_table.findNeighborOb(current_point.x, current_point.y);
        int temp_ob;
        int ob_kind;
        //qDebug() << temp_pair.first << temp_pair.second;
        if(terminal_point.y > current_point.y)
        {
            temp_ob = temp_pair.second;
            ob_kind = 2;
        }
        else
        {
            temp_ob = temp_pair.first;
            ob_kind = 1;
        }
        //make line
        if(temp_ob == -1)
        {
            current_direction = 0;
            Point next_point(current_point.x, terminal_point.y);
            Line new_line(current_point, next_point);
            new_line.makeShort();
            line_stack.push_back(new_line);
            current_point = next_point;
        }
        else
        {
            Obstacle ob = ob_vector[temp_ob];
            int next_y = (ob_kind==2)?ob.y1:ob.y2;
            Point next_point(current_point.x, next_y);
            Line new_line(current_point, next_point);
            new_line.makeShort();
            line_stack.push_back(new_line);
            current_point = next_point;
            current_direction = 0;

            awayOb(ob.x1, ob.x2);
        }
    }
    return;
}

int LineMaker::awayOb(int a, int b)
{
    if(current_direction == 0)
    {
        int next_x_or_y = a, the_other_x_or_y = b;
        if(abs(terminal_point.x-a) > abs(terminal_point.x-b))
        {
            next_x_or_y = b;
            the_other_x_or_y = a;
        }
        Point the_other_point(the_other_x_or_y, current_point.y);
        Point next_point(next_x_or_y, current_point.y);
        Line new_line(current_point, next_point);
        new_line.next_line = new Line(current_point, the_other_point);
        line_stack.push_back(new_line);
        current_point = next_point;
        current_direction = 1;
        while(ob_x_table.findNeighborOb(current_point.x, current_point.y).first == -2)
        {
            retry();
        }
    }
    else if(current_direction == 1)
    {
        int next_x_or_y = a, the_other_x_or_y = b;
        if(abs(terminal_point.y-a) > abs(terminal_point.y-b))
        {
            next_x_or_y = b;
            the_other_x_or_y = a;
        }
        Point the_other_point(current_point.x, the_other_x_or_y);
        Point next_point(current_point.x, next_x_or_y);
        Line new_line(current_point, next_point);
        new_line.next_line = new Line(current_point, the_other_point);
        line_stack.push_back(new_line);
        current_point = next_point;
        current_direction = 0;
        while(ob_y_table.findNeighborOb(current_point.x, current_point.y).first == -2)
        {
            retry();
        }
    }
}

void LineMaker::retry()
{
    Line bad_line = line_stack[line_stack.size()-1];
    line_stack.pop_back();
    while(bad_line.next_line == 0)
    {
        line_stack.pop_back();
        bad_line = line_stack[line_stack.size()-1];
        line_stack.pop_back();
    }
    line_stack.push_back(*(bad_line.next_line));
    current_point = bad_line.next_line->end_point;
    current_direction = 1- bad_line.next_line->direction;
}

int LineMaker::isArrive()
{
    if((current_point.x == terminal_point.x) && (current_point.y == terminal_point.y))
        return 1;
    return 0;
}

//output
void LineMaker::showLine()
{
    QFile outFile("debug.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    int size = line_stack.size();
    ts<<"\nall line:";
    ts<<size<<endl;
    for(int i=0;i<size;i++)
    {
        ts<<line_stack[i].end_point.x<<" "<<line_stack[i].end_point.y<<endl;
    }
}

int LineMaker::showLenth()
{
    QFile outFile("debug.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    int size = line_stack.size();
    int lenth = 0;
    for(int i=0;i<size;i++)
    {
        lenth += line_stack[i].lenth;
    }
    ts<<"\nlenth = "<<lenth<<endl;
    int distance = abs(init_point.x - terminal_point.x) + abs(init_point.y - terminal_point.y);
    ts<<"rate = "<<(double)lenth/distance<<endl;
    return lenth;
}
