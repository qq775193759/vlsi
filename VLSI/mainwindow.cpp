#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QInputDialog>
#include <qdebug.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Point init(50,50);
    /*Point term(400,400);
    Obstacle ob1(100,210,40,160);
    Obstacle ob2(190,310,90,210);*/
    Point term(200,400);
    //Obstacle ob1(100,120,40,60);
    //Obstacle ob2(190,210,90,110);

    std::vector<Obstacle> ob_table;
    //ob_table.push_back(ob1);
    //ob_table.push_back(ob2);
    linemaker = new LineMaker(init, term, ob_table);
    linemaker->Try();

    linemaker->showLine();

    update();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *e)
{
    drawAll();
}

void MainWindow::drawLine(Line l)
{
    QPainter* painter = new QPainter(this);
    QPen pen(Qt::red,2);
    painter->setPen(pen);
    painter->drawLine(l.start_point.x, l.start_point.y, l.end_point.x, l.end_point.y);
    //painter->restore();
}

void MainWindow::drawOb(Obstacle o)
{
    QPainter* painter = new QPainter(this);

    /*QBrush brush1(Qt::yellow);
    QPen pen1(Qt::yellow, 0);
    painter->setBrush(brush1);
    painter->setPen(pen1);
    painter->drawRect(o.x1, o.y1, o.x2 - o.x1, o.y2 - o.y1);*/

    QBrush brush2(Qt::blue);
    QPen pen2(Qt::blue, 0);
    painter->setBrush(brush2);
    painter->setPen(pen2);
    painter->drawRect(o.x1+5, o.y1+5, o.x2 - o.x1 - 10, o.y2 - o.y1 - 10);
    //painter->restore();
}

void MainWindow::drawPoi(Point p)
{
    QPainter* painter = new QPainter(this);
    QPen pen(Qt::red, 2);
    painter->setPen(pen);
    painter->drawEllipse(p.x-3,p.y-3,6,6);
    //painter->restore();
}

void MainWindow::drawAll()
{
    for(int i=0;i<linemaker->line_stack.size();i++)
    {
        drawLine(linemaker->line_stack[i]);
    }
    for(unsigned int i=0;i<linemaker->ob_vector.size();i++)
    {
        drawOb(linemaker->ob_vector[i]);
    }
    drawPoi(linemaker->init_point);
    drawPoi(linemaker->terminal_point);

    QPainter* painter = new QPainter(this);
    QPen pen(Qt::blue,2);
    painter->setPen(pen);
    painter->drawRect(40, 40, 500, 500);
    //painter->restore();
}

void MainWindow::on_actionClear_triggered()
{
    linemaker->clear();
    linemaker->ob_vector.clear();
    update();
}

void MainWindow::on_action_entersrc_triggered()
{
    bool ok1,ok2;
    int x=QInputDialog::getInt(this,tr("输入x坐标"),tr("请输入源点的x坐标"),0,0,400,10,&ok1);
    int y=QInputDialog::getInt(this,tr("输入y坐标"),tr("请输入源点的y坐标"),0,0,400,10,&ok2);
    if(ok1&&ok2)
    {
       linemaker->init_point = Point(x,y);
       linemaker->clear();
       linemaker->Try();
       linemaker->showLine();
       update();
    }
}

void MainWindow::on_action_enterdest_triggered()
{
    bool ok1,ok2;
    int x=QInputDialog::getInt(this,tr("输入x坐标"),tr("请输入源点的x坐标"),0,0,400,10,&ok1);
    int y=QInputDialog::getInt(this,tr("输入y坐标"),tr("请输入源点的y坐标"),0,0,400,10,&ok2);
    if(ok1&&ok2)
    {
        linemaker->terminal_point = Point(x,y);
        linemaker->clear();
        linemaker->Try();
        linemaker->showLine();
        update();
    }
}


void MainWindow::on_pushButton_add_clicked()
{
   int x1 = ui->textEdit_x1->toPlainText().toInt();
   int y1 = ui->textEdit_y1->toPlainText().toInt();
   int x2 = ui->textEdit_x2->toPlainText().toInt();
   int y2 = ui->textEdit_y2->toPlainText().toInt();
    Obstacle ob(x1-5, x2+5, y1-5, y2+5);
    linemaker->ob_vector.push_back(ob);
    linemaker = new LineMaker(linemaker->init_point, linemaker->terminal_point, linemaker->ob_vector);
    linemaker->Try();
    linemaker->showLine();
    update();
}
