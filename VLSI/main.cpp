#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <fstream>
#include <time.h>

#include <linemaker.h>
#include <qdebug.h>

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        abort();
    }
    QFile outFile("debug.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << msg << endl;

}

int main(int argc, char *argv[])
{
    /*QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();*/
    QFile outFile("debug.log");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);

    std::vector<Obstacle> ob_table;
    std::ifstream fin("test.txt");
    int a,b;
    fin>>a>>b;
    Point init(a,b);
    fin>>a>>b;
    Point term(a,b);
    int obs_n;
    fin>>obs_n;
    for(int i=1;i<=obs_n;i++)
    {
        int x1,x2,y1,y2;
        fin>>x1>>x2>>y1>>y2;
        Obstacle* ob = new Obstacle(x1,x2,y1,y2);
        ob_table.push_back(*ob);
    }
    LineMaker* linemaker = new LineMaker(init, term, ob_table);
    ts<<"start clock = "<<clock()<<endl;
    linemaker->Try();
    ts<<"end clock ="<<clock()<<endl;

    linemaker->showLine();
    linemaker->showLenth();
}
